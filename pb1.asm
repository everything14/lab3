.386
.model flat, stdcall
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;includem biblioteci, si declaram ce functii vrem sa importam
includelib msvcrt.lib
extern exit: proc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;declaram simbolul start ca public - de acolo incepe executia
public start
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;sectiunile programului, date, respectiv cod
.data
;aici declaram date

.code
start:
	;aici se scrie codul
	
	; initializam cu valoare random
	mov eax, 1
	mov ebx, 1
	
	; 7 * eax
	mov ecx, 7
	sub edx, edx
	mul ecx
	mov esi, eax ; esi = 7 * eax
	
	; 2 * ebx
	mov eax, ebx
	mov ecx, 2
	sub edx, edx
	mul ecx
	sub esi, eax ; esi = 7 * eax - 2 * ebx
	
	; ebx / 8
	mov eax, ebx
	mov ecx, 8
	sub edx, edx
	div ecx
	sub esi, eax ; esi = 7 * eax - 2 * ebx - ebx / 8
	
	mov eax, esi
	
	;terminarea programului
	push 0
	call exit
end start
